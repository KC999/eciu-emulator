#Author: Kumar A Chaya
#Date: 01/26/2021


import serial
import time

SerialPort = serial.Serial()
SerialPort.port = "COM13"
SerialPort.baudrate = 9600

Meter_Num = "0x383332475442"[2:]
bytes_object = bytes.fromhex(hex_string)
Meternum_ascii_string = bytes_object.decode("ASCII")

volt = "120000"
curr = "5"
pwr = "600"


def OpenSerailPort():
    try:
        global SerialPort
        if(SerialPort.isOpen() == False):
            SerialPort.timeout = 1
            SerialPort.open()
        time.sleep(.5)
        print("Electricity meter port opened")
    except IOError:
        SerialPort.close()
        SerialPort.open()
        print("port was already opended, was closed and opened it again!")
    except Exception as e:
        template = "ERROR: An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__,e.args)
        print(message)
        time.sleep(1)
        SerialPort.close()
        time.sleep(1)
        SerialPort.open()
        time.sleep(1)
        print("Re-opened serial port!")
    return SerialPort.isOpen()


def sendCommand(cmd):
#	log.both("TX: " + cmd)	
	SerialPort.write((cmd + "\r").encode());
	SerialPort.flush()


def readResponse():
    line1 = "".encode()
    while (SerialPort.inWaiting()>0):
	    line = SerialPort.readline()
	    line1 += line
##    print(line1.decode())
    return line1.decode()
   


while True:
    inpt = readResponse()

    if "VOLTAJE A" in inpt: #voltage A in English
        sendCommand('A' + volt)

    if "CORRIENTE A" in inpt:
        sendCommand('A' + curr)

    if "POTENCIA A" in inpt:
        sendCommand('A' + pwr)
    
    
    
